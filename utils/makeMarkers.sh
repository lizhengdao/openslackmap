#!/bin/bash

wget -O reuhigh.kml "https://mapsengine.google.com/map/kml?mid=znqWYvscx18o.kpHqF8APSOOs&lid=znqWYvscx18o.kjvw8cXAE3t8&forcekml=1"
gpsbabel -i kml -f reuhigh.kml -o gpx -F reuhigh.gpx

wget -O reuwater.kml "https://mapsengine.google.com/map/kml?mid=znqWYvscx18o.kpHqF8APSOOs&lid=znqWYvscx18o.kWAepTWxbiQk&forcekml=1"
gpsbabel -i kml -f reuwater.kml -o gpx -F reuwater.gpx

wget -O reulong.kml "https://mapsengine.google.com/map/kml?mid=znqWYvscx18o.kpHqF8APSOOs&lid=znqWYvscx18o.kjvmzIE1XBdM&forcekml=1"
gpsbabel -i kml -f reulong.kml -o gpx -F reulong.gpx

wget -O reumid.kml "https://mapsengine.google.com/map/kml?mid=znqWYvscx18o.kpHqF8APSOOs&lid=znqWYvscx18o.kB5TF8XhLURo&forcekml=1"
gpsbabel -i kml -f reumid.kml -o gpx -F reumid.gpx

wget -O high.kml "https://mapsengine.google.com/map/kml?mid=zORQyMdozCF8.kSxmb-A8GQIo&forcekml=1"
gpsbabel -i kml -f high.kml -o gpx -F high.gpx

wget -O long.kml "https://mapsengine.google.com/map/kml?mid=zGLic3RX4K4Q.kEzorLy357Yw&lid=zGLic3RX4K4Q.kEuo7jo3E6oI&forcekml=1"
gpsbabel -i kml -f long.kml -o gpx -F long.gpx

wget -O slack.kml "https://mapsengine.google.com/map/kml?mid=zGLic3RX4K4Q.kEzorLy357Yw&lid=zGLic3RX4K4Q.kT5mmZmqKMFA&forcekml=1"
gpsbabel -i kml -f slack.kml -o gpx -F slack.gpx

wget -O water.kml "https://mapsengine.google.com/map/kml?mid=z4yvtFE6n6E4.k0gapG_DdBdA&forcekml=1"
gpsbabel -i kml -f water.kml -o gpx -F water.gpx

../openslackmap.py reuhigh.gpx
../openslackmap.py reumid.gpx
../openslackmap.py reulong.gpx
../openslackmap.py reuwater.gpx
../openslackmap.py high.gpx
../openslackmap.py water.gpx
../openslackmap.py long.gpx
../openslackmap.py slack.gpx

rm water.kml high.kml long.kml slack.kml reuhigh.kml reulong.kml reuwater.kml reumid.kml

mv reuhigh.gpx.markers reuhighmarkers.txt
mv reulong.gpx.markers reulongmarkers.txt
mv reumid.gpx.markers reumidmarkers.txt
mv reuwater.gpx.markers reuwatermarkers.txt
mv high.gpx.markers highmarkers.txt
mv water.gpx.markers watermarkers.txt
mv long.gpx.markers longmarkers.txt
mv slack.gpx.markers slackmarkers.txt
