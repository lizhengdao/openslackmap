#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, math, os
import json
import codecs
import re
import HTMLParser
import gpxpy, gpxpy.gpx, geojson

h = HTMLParser.HTMLParser()

def format_time_seconds(time_s):
    if not time_s:
        return 'n/a'
    minutes = math.floor(time_s / 60.)
    hours = math.floor(minutes / 60.)

    return '%s:%s:%s' % (str(int(hours)).zfill(2), str(int(minutes % 60)).zfill(2), str(int(time_s % 60)).zfill(2))

def distance(p1, p2):
    """ return distance between these two gpx points in meters
    """

    lat1 = p1.latitude
    long1 = p1.longitude
    lat2 = p2.latitude
    long2 = p2.longitude

    if (lat1 == lat2 and long1 == long2):
        return 0

    # Convert latitude and longitude to
    # spherical coordinates in radians.
    degrees_to_radians = math.pi/180.0

    # phi = 90 - latitude
    phi1 = (90.0 - lat1)*degrees_to_radians
    phi2 = (90.0 - lat2)*degrees_to_radians

    # theta = longitude
    theta1 = long1*degrees_to_radians
    theta2 = long2*degrees_to_radians

    # Compute spherical distance from spherical coordinates.

    # For two locations in spherical coordinates
    # (1, theta, phi) and (1, theta, phi)
    # cosine( arc length ) =
    #    sin phi sin phi' cos(theta-theta') + cos phi cos phi'
    # distance = rho * arc length

    cos = (math.sin(phi1)*math.sin(phi2)*math.cos(theta1 - theta2) +
           math.cos(phi1)*math.cos(phi2))
    arc = math.acos( cos )

    # Remember to multiply arc by the radius of the earth
    # in your favorite set of units to get length.
    return arc*6371000

def getMarkersFromGpx(gpx_content):

    regl = 'L\s?:.*'
    rep = re.compile(regl)

    reglink = '(http://waterlinedatabase.blogspot.fr[^\(.]+\.html)'
    replink = re.compile(reglink)

    reglinkh = '(http://highlinedatabase.blogspot.com[^\(.]+\.html)'
    replinkh = re.compile(reglinkh)

    reglinkhfr = '(http://highlinedatabase.blogspot.fr[^\(.]+\.html)'
    replinkhfr = re.compile(reglinkhfr)

    markers = '{"markers" : [\n'
    gpx = gpxpy.parse(gpx_content)

    for waypoint in gpx.waypoints:
        length = []
        height = '"unknown"'
        name = waypoint.name
        if 'L :' in name:
            ltab = name.split('L :')[1:]
            for l in ltab:
                lsubtab = l.split('H :')[0].replace(',',' ').replace('-',' ').replace('m',' ').split()
                length.extend(lsubtab)
        if 'H :' in name:
            height = name.split('H :')[-1].strip().replace('m',' ').split()[0]

        purified_name = re.sub(rep,'',name).strip().strip('-').strip().replace('"',"'")

        # get length in first comment line
        for commentline in waypoint.comment.split('<br>'):
            cl = commentline.replace('- L:', 'L : ')
            clpure = cl.replace('- H:', 'H : ')
            if clpure.startswith('L : '):
                ltab = clpure.split('L :')[1:]
                for l in ltab:
                    lsubtab = l.split('H :')[0].replace(',',' ').replace('-',' ').replace('m',' ').split()
                    for ele in lsubtab:
                        if ele.isdigit():
                            length.append(ele)
            if 'Longueur : ' in clpure:
                l = clpure.split('Longueur : ')[-1].split('m')[0]
                if l.isdigit():
                    length.append(l)
            if clpure.startswith('H : '):
                h = name.split('H :')[-1].strip().replace('m',' ').split()[0]
                if h.isdigit():
                    height = h

        if not length:
            length = ['"unknown"']
        for l in length:
            mar = u'["%s", %s, %s, "%s", %s, %s],\n'%(
                            purified_name,
                            waypoint.longitude, 
                            waypoint.latitude,
                            #h.unescape(waypoint.comment).replace('"',"'"),
                            waypoint.comment.replace('"',"'").strip().replace('\n',''),
                            l,
                            height
                        )

            markers += mar

    markers += '] }'

    markers = re.sub(replink, "<a href='\\1' target='_blank'>Details</a>", markers)
    markers = re.sub(replinkh, "<a href='\\1' target='_blank'>Details</a>", markers)
    markers = re.sub(replinkhfr, "<a href='\\1' target='_blank'>Details</a>", markers)
    return markers


if __name__ == "__main__":
    path = sys.argv[1]
    if not os.path.exists(path):
        sys.stderr.write('%s does not exist'%path)
        sys.exit(1)

    markers = []

    fd = open(path, 'r')
    content = fd.read()
    fd.close()

    gf = codecs.open('%s.markers'%path, 'w', encoding="utf-8")
    gf.write(getMarkersFromGpx(content))
    gf.close()
