OpenSlackMap, alternative web interface to explore Slackline databases.
=================

# Introduction
---
OpenSlackMap is a free software which can display all referenced slackline spots in other maps. It adds functionnalities like
sorting, filters, routing, export to GPX...

OpenSlackMap was born because google map is so slow on a two-years-old laptop and also because of my hesitation
over giving too much location information to google...

OpenSlackMap takes his data from the KML files downloadable from the google maps of [highlinedatabase](http://highlinedatabase.blogspot.fr/)
, [waterlinedatabase](http://waterlinedatabase.blogspot.fr/), [Slackline974](http://www.slackline974.org/map/) and [SlackMountain](http://slack-mountain.com/shop/fr/content/14-carte). These KML files
are converted to GPX format with [GPSBabel](http://www.gpsbabel.org/) and displayed in a web interface using [Leaflet](http://leafletjs.com).

OpenSlackMap is faster than google map, it gives many more map layers (IGN, OpenStreetMap, ESRI Satellite...), it gives a clean view by grouping close spots, and most of all,
it is a free software that anybody can use, host, study, modify, improve... Create an issue or send a merge request if you have good ideas !

OpenSlackMap is completly functionnal and **still under development.**

Here is [my openslackmap instance](http://openslackmap.pluton.cassio.pe)

OpenSlackMap uses [Leaflet](http://leafletjs.com),
[Leaflet.markercluster](https://github.com/Leaflet/Leaflet.markercluster),
[Leaflet.Elevation](https://github.com/MrMufflon/Leaflet.Elevation),
[Leaflet.sidebar-v2](https://github.com/turbo87/sidebar-v2/),
[Leaflet Control Geocoder](https://github.com/perliedman/leaflet-control-geocoder),
[Leaflet.MiniMap](https://github.com/Norkart/Leaflet-MiniMap),
[Leaflet.activearea](https://github.com/Mappy/Leaflet-active-area)
[Leaflet.routing.machine](https://github.com/perliedman/leaflet-routing-machine)
[Leaflet Mouse Position](https://github.com/ardhi/Leaflet.MousePosition),
[JQuery](http://jquery.com/), [python-geojson](https://github.com/frewsxcv/python-geojson),
[gpxpy](https://github.com/tkrajina/gpxpy)...

## Table of content
---
1. [Screenshots](#screenshots)
2. [Requirements](#requirements)
3. [Installation](#installation)
4. [Usage](#usage)

# Screenshots
---
Beautifull, fast and easy navigation with markercluster plugin :
![openslackmap screenshot1](https://gitlab.com/eneiluj/openslackmap/raw/master/screenshots/markers.png)

Marker popup with line description :
![openslackmap screenshot2](https://gitlab.com/eneiluj/openslackmap/raw/master/screenshots/osm.png)

Many map layers including satellite view, IGN...
![openslackmap screenshot3](https://gitlab.com/eneiluj/openslackmap/raw/master/screenshots/sat.png)

Hover on a line name directly show it on the map with a violet marker :
![openslackmap screenshot3](https://gitlab.com/eneiluj/openslackmap/raw/master/screenshots/violet.png)

# Requirements
---
You'll need [gpxpy](https://github.com/tkrajina/gpxpy) to be installed on your web server.

You'll also need a web server and PHP 5.

# Installation
---
Copy "www" folder somewhere in your webserver path.

Copy openslackmap.py somewhere outside your webserver path.

Launch makemarkers.sh script to download data and generate marker files. I personnaly run it every day with a crontab. Adapt it to find openslackmap.py .

# Usage
---
Basically it displays the map with the slackline spots.

In the side panel, a table lists all visible line markers by showing its name, length and height.
Hover on a row to quickly display the line marker without changing map position or zoom.
Click on the line name to center map on it.

Click on line names or markers for detailed information and sometimes to get the link to the corresponding blogspot post.

This sidebar table is sortable by column.

It is possible to restrict display to highlines or waterlines only and to filter on length and height min and max values.

In line description popup, click on "get routing info" to set destination in the bottom-right routing widget. Then set your origin and get the route. It is possible to add checkpoints to a route by dragging a point or by clicking the + button.

