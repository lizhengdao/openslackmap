var openslackmap = {
    map: {},
    markers: [],
    markersPopupTxt: {},
    markerLayer: null,
    markerLayerByName: {},
    minimapControl: null,
    searchControl: null,
    routeControl: null,
    tablesortCol: [2,1],
    currentBigLayer : null,
    currentHoverLayer : null,
};

var tabType = {
    'l':'longline',
    's':'slackline',
    'w':'waterline',
    'h':'highline'
}

/*
 * markers are stored as list of values in this format :
 *
 * m[0] : name,
 * m[1] : lon,
 * m[2] : lat,
 * m[3] : comment,
 * m[4] : length,
 * m[5] : height,
 *
 */

var LAT = 2;
var LON = 1;
var NAME = 0;
var COMMENT = 3;
var LENGTH = 4;
var HEIGHT = 5;
var TYPE = 6;

function load()
{
    load_map();
}

function load_map() {
  openslackmap.map = new L.Map('map', {zoomControl: true}).setActiveArea('activeArea');
  L.control.scale({metric: true, imperial: true, position:'topleft'}).addTo(openslackmap.map);
  L.control.mousePosition().addTo(openslackmap.map);
  openslackmap.searchControl = L.Control.geocoder({position:'topleft'});
  openslackmap.searchControl.addTo(openslackmap.map);
  openslackmap.locateControl = L.control.locate({follow:true});
  openslackmap.locateControl.addTo(openslackmap.map);
  L.control.sidebar('sidebar').addTo(openslackmap.map);

  //L.Routing.control({ position:'bottomright',
  //}).addTo(openslackmap.map);

openslackmap.routeControl = L.Routing.control({ position:'bottomright',
    plan: L.Routing.plan([
        //L.latLng(48.8588,2.3469),
        //L.latLng(52.3546,4.9039)
        null,null
    ], {
        createMarker: function(i, wp) {
            return L.marker(wp.latLng, {
                draggable: true,
                icon: new L.Icon.Label.Default({iconUrl:'js/images/marker-icon-yellow.png', labelText: String.fromCharCode(65 + i) })
            });
        },
        geocoder: L.Control.Geocoder.nominatim(),
        routeWhileDragging: true
    }),
    routeWhileDragging: true,
    routeDragTimeout: 250,
    fitSelectedRoutes: 'smart'
});
openslackmap.routeControl.addTo(openslackmap.map);

  // get url from key and layer type
  function geopUrl (key, layer, format)
  { return "http://wxs.ign.fr/"+ key + "/wmts?LAYER=" + layer
      +"&EXCEPTIONS=text/xml&FORMAT="+(format?format:"image/jpeg")
          +"&SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&STYLE=normal"
          +"&TILEMATRIXSET=PM&TILEMATRIX={z}&TILECOL={x}&TILEROW={y}" ;
  }
  // change it if you deploy openslackmap
  var API_KEY = "ljthe66m795pr2v2g8p7faxt";
  var ign = new L.tileLayer ( geopUrl(API_KEY,"GEOGRAPHICALGRIDSYSTEMS.MAPS"),
          { attribution:'&copy; <a href="http://www.ign.fr/">IGN-France</a>',
              maxZoom:18
          });

  var osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
  var osmAttribution = 'Map data &copy; 2013 <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
  var osm = new L.TileLayer(osmUrl, {maxZoom: 18, attribution: osmAttribution});

  var osmfrUrl = 'http://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png';
  var osmfr = new L.TileLayer(osmfrUrl, {maxZoom: 20, attribution: osmAttribution});
  var osmfr2 = new L.TileLayer(osmfrUrl, {minZoom: 0, maxZoom: 13, attribution: osmAttribution});

  var openmapsurferUrl = 'http://openmapsurfer.uni-hd.de/tiles/roads/x={x}&y={y}&z={z}';
  var openmapsurferAttribution = 'Imagery from <a href="http://giscience.uni-hd.de/">GIScience Research Group @ University of Heidelberg</a> &mdash; Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>';
  var openmapsurfer = new L.TileLayer(openmapsurferUrl, {maxZoom: 18, attribution: openmapsurferAttribution});

  var transportUrl = 'http://a.tile2.opencyclemap.org/transport/{z}/{x}/{y}.png';
  var transport = new L.TileLayer(transportUrl, {maxZoom: 18, attribution: osmAttribution});

  var pisteUrl = 'http://tiles.openpistemap.org/nocontours/{z}/{x}/{y}.png';
  var piste = new L.TileLayer(pisteUrl, {maxZoom: 18, attribution: osmAttribution});

  var hikebikeUrl = 'http://toolserver.org/tiles/hikebike/{z}/{x}/{y}.png';
  var hikebike = new L.TileLayer(hikebikeUrl, {maxZoom: 18, attribution: osmAttribution});

  var osmCycleUrl = 'http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png';
  var osmCycleAttrib = '&copy; <a href="http://www.opencyclemap.org">OpenCycleMap</a>, &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>';
  var osmCycle = new L.TileLayer(osmCycleUrl, {maxZoom: 18, attribution: osmCycleAttrib});

  var darkUrl = 'http://a.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}.png';
  var darkAttrib = '&copy; Map tiles by CartoDB, under CC BY 3.0. Data by OpenStreetMap, under ODbL.';
  var dark = new L.TileLayer(darkUrl, {maxZoom: 18, attribution: darkAttrib});

  var esriTopoUrl = 'http://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}';
  var esriTopoAttrib = 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ, TomTom, Intermap, iPC, USGS, FAO, NPS, NRCAN, GeoBase, Kadaster NL, Ordnance Survey, Esri Japan, METI, Esri China (Hong Kong), and the GIS User Community';
  var esriTopo = new L.TileLayer(esriTopoUrl, {maxZoom: 18, attribution: esriTopoAttrib});

  var esriAerialUrl = 'http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}';
  var esriAerialAttrib = 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community';
  var esriAerial = new L.TileLayer(esriAerialUrl, {maxZoom: 18, attribution: esriAerialAttrib});

  var tonerUrl = 'http://{s}.tile.stamen.com/toner/{z}/{x}/{y}.jpg';
  var stamenAttribution = '<a href="http://leafletjs.com" title="A JS library for interactive maps">Leaflet</a> | © Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>, Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, under <a href="http://creativecommons.org/licenses/by-sa/3.0">CC BY SA</a>.';
  var toner = new L.TileLayer(tonerUrl, {maxZoom: 18, attribution: stamenAttribution});

  var watercolorUrl = 'http://{s}.tile.stamen.com/watercolor/{z}/{x}/{y}.jpg';
  var watercolor = new L.TileLayer(watercolorUrl, {maxZoom: 18, attribution: stamenAttribution});

  var routeUrl = 'http://{s}.tile.openstreetmap.fr/route500/{z}/{x}/{y}.png';
  var routeAttrib = '&copy, Tiles © <a href="http://www.openstreetmap.fr">OpenStreetMap France</a>';
  var route = new L.TileLayer(routeUrl, {minZoom: 1, maxZoom: 20, attribution: routeAttrib});

  openslackmap.map.setView(new L.LatLng(27, 5), 3);
  
  var baseLayers = {
        "OpenStreetMap": osm,
        "OpenCycleMap": osmCycle,
        "IGN France": ign,
        "OpenMapSurfer Roads": openmapsurfer,
        "Hike & bike": hikebike,
        "OSM Transport": transport,
        "ESRI Aerial": esriAerial,
        "ESRI Topo with relief": esriTopo,
        "Dark" : dark,
        "Toner" : toner,
        "Watercolor" : watercolor,
        "OpenStreetMap France": osmfr,
  };
  var baseOverlays = {
      'OsmFr Route500': route,
      'OpenPisteMap Relief': L.tileLayer('http://tiles2.openpistemap.org/landshaded/{z}/{x}/{y}.png', {
              attribution: '&copy, Tiles © <a href="http://www.openstreetmap.fr">OpenStreetMap France</a>',
              minZoom: 1, maxZoom: 15
              }),
      'OpenPisteMap pistes' : piste,
  };

  new L.control.layers(baseLayers, baseOverlays).addTo(openslackmap.map);

  openslackmap.minimapControl = new L.Control.MiniMap(osmfr2, { toggleDisplay: true, position:'bottomleft' }).addTo(openslackmap.map);
  openslackmap.minimapControl._toggleDisplayButtonClicked();

  openslackmap.map.addLayer(osmfr);

  //openslackmap.map.on('contextmenu',rightClick);
  openslackmap.map.on('popupclose',function() {});
  //openslackmap.map.on('viewreset',updateLineListFromBounds);
  //openslackmap.map.on('dragend',updateLineListFromBounds);
  openslackmap.map.on('moveend',updateLineListFromBounds);
  openslackmap.map.on('zoomend',updateLineListFromBounds);
}

function rightClick(e) {
    //new L.popup()
    //    .setLatLng(e.latlng)
    //    .setContent(preparepopup(e.latlng.lat,e.latlng.lng))
    //    .openOn(openslackmap.map);
}

function removeMarkers(){
    if (openslackmap.markerLayer != null){
        openslackmap.map.removeLayer(openslackmap.markerLayer);
        delete openslackmap.markerLayer;
        openslackmap.markerLayer = null;
    }
}

// add markers respecting the filtering rules
function addMarkers(){
    var markerclu = L.markerClusterGroup({ chunkedLoading: true });

    for (var i = 0; i < openslackmap.markers.length; i++) {
        var a = openslackmap.markers[i];
        if (filter(a)){
            var title = a[NAME];
            var marker = L.marker(L.latLng(a[LAT], a[LON]), { title: title });
            marker.bindPopup(openslackmap.markersPopupTxt[title],{autoPan:true, maxWidth:400});
            openslackmap.markerLayerByName[title] = marker;
            markerclu.addLayer(marker);
        }
    }

    openslackmap.map.addLayer(markerclu);
    //openslackmap.map.setView(new L.LatLng(47, 3), 2);

    openslackmap.markerLayer = markerclu;

    //markers.on('clusterclick', function (a) {
    //   var bounds = a.layer.getConvexHull();
    //   updateLineListFromBounds(bounds);
    //});
}

// return true if the marker respects all filters
function filter(m){
    var mhei = m[HEIGHT];
    var mlen = m[LENGTH];
    var mtype = m[TYPE];
    var lenmin = $('#lenmin').val();
    var lenmax = $('#lenmax').val();
    var heimin = $('#heimin').val();
    var heimax = $('#heimax').val();
    var seltype = $('select#categoryselect').val();

    if (seltype == 'Longline only' && mtype != 'l'){
        return false;
    }
    if (seltype == 'Slackline only' && mtype != 's'){
        return false;
    }
    if (seltype == 'Waterline only' && mtype != 'w'){
        return false;
    }
    if (seltype == 'Highline only' && mtype != 'h'){
        return false;
    }
    if (lenmin != ''){
        if (mlen < lenmin){
            return false;
        }
    }
    if (lenmax != ''){
        if (lenmax < mlen){
            return false;
        }
    }
    if (heimin != ''){
        if (mhei < heimin){
            return false;
        }
    }
    if (heimax != ''){
        if (heimax < mhei){
            return false;
        }
    }

    return true;
}

function clearFiltersValues(){
    $('#lenmin').val('');
    $('#lenmax').val('');
    $('#heimin').val('');
    $('#heimax').val('');
}

function updateLineListFromBounds(e){

    var m;
    var table_rows = "";
    var mapBounds = openslackmap.map.getBounds();
    for (var i = 0; i < openslackmap.markers.length; i++) {
        m = openslackmap.markers[i];
        if (filter(m)){
            if (mapBounds.contains(new L.LatLng(m[LAT], m[LON]))){
                table_rows = table_rows + "<tr><td class='trackname'><div class='trackcol'>";
                // TODO : trigger popup for this line
                table_rows = table_rows + '<a href="" target="_blank" class="linetablelink" name="'+m[NAME]+'">'+m[NAME]+'</a>\n';
                table_rows = table_rows +' <a class="permalink" title="permalink" target="_blank" href="?line='+m[NAME].replace(/\s+/g,'')+'">[p]</a></div></td>\n';
                if (m[TYPE] == 'w'){
                    table_rows = table_rows + "<td>waterline</td>\n";
                }
                else if (m[TYPE] == 'l'){
                    table_rows = table_rows + "<td>longline</td>\n";
                }
                else if (m[TYPE] == 's'){
                    table_rows = table_rows + "<td>slackline</td>\n";
                }
                else{
                    table_rows = table_rows + "<td>highline</td>\n";
                }
                table_rows = table_rows + "<td>"+m[LENGTH]+"</td>\n";
                table_rows = table_rows + "<td>"+m[HEIGHT]+"</td>\n";
                table_rows = table_rows + "</tr>\n";
            }
        }
    }

    if (table_rows == ""){
        var table = "None";
        $('#linelist').html(table);
    }
    else{
        var table = "<table id='linetable' class='tablesorter'>\n<thead>";
        table = table + "<tr>";
        table = table + "<th>Line</th>\n";
        table = table + "<th>Type</th>\n";
        table = table + "<th>Length</th>\n";
        table = table + "<th>Height</th>\n";
        table = table + "</tr></thead><tbody>\n";
        table = table + table_rows;
        table = table + "</tbody></table>";
        $('#linelist').html(table);
        $('#linetable').tablesorter({
            widthFixed: false,
            sortList: [openslackmap.tablesortCol],
            dateFormat: "yyyy-mm-dd",
            headers: {
                2: {sorter: "digit", string: "min"},
                3: {sorter: "digit", string: "min"},
            }
        });
    }
}

/*
 * display markers if the checkbox is checked
 */
function redraw()
{
    // remove markers if they are present
    removeMarkers();
    addMarkers();
    return;

}

function genPopupTxt(){
    for (var i = 0; i < openslackmap.markers.length; i++) {
        var a = openslackmap.markers[i];
        var title = a[NAME];
        popupTxt = '<h3 style="text-align:center;">Line : <a href="?line='+title+'" target="_blank">'+title+'</a></h3><hr/>';
        popupTxt = popupTxt + '<a href="?line='+title.replace(/\s+/g,'')+'" target="_blank">Permalink</a> ';
        popupTxt = popupTxt +'<button class="routebutton" name="'+title+'" coord="'+a[LAT]+','+a[LON]+'">Get routing info</button>';
        popupTxt = popupTxt +'<ul>';
        popupTxt = popupTxt +'<li><b>Type</b> : '+tabType[a[TYPE]]+'</li>';
        popupTxt = popupTxt +'<li><b>Length</b> : '+a[LENGTH]+' m</li>';
        popupTxt = popupTxt +'<li><b>Heigh</b> : '+a[HEIGHT]+' m</li>';
        popupTxt = popupTxt +'<li><b>Comment :</b><div style="overflow-y:scroll;max-height: 250px"> '+a[COMMENT]+'</div></li>';
        popupTxt = popupTxt + '</ul>';

        openslackmap.markersPopupTxt[title] = popupTxt;
    }
}

function checkKey(e){
    e = e || window.event;
    var kc = e.keyCode;
    console.log(kc);

    if (kc == 0 || kc == 176 || kc == 192){
        e.preventDefault();
        openslackmap.searchControl._toggle();
    }
    if (kc == 161 || kc == 223){
        e.preventDefault();
        openslackmap.minimapControl._toggleDisplayButtonClicked();
    }
    if (kc == 60 || kc == 220){
        e.preventDefault();
        $('#sidebar').toggleClass('collapsed');
        if ($('#sidebar li.active').length == 0){
            $('#sidebar li.previousactive').addClass('active');
            $('#sidebar li.previousactive').removeClass('previousactive');
        }
        else{
            $('#sidebar li.active').addClass('previousactive');
            $('#sidebar li.active').removeClass('active');
        }
    }
}

function getUrlParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}

// TODO make marker bigger
function displayOnHover(tr){
    //var tid = tr.find('.drawtrack').attr('id');
    //$.ajax({url: "getGeoJson.php?subfolder="+openslackmap.subfolder+"&track="+tid}).done(
    //        function(msg){addHoverTrackDraw(msg)});
}

function findMarkerByName(name){
    for (var i = 0; i < openslackmap.markers.length; i++){
        if (openslackmap.markers[i][NAME] == 'Agropolis - Le Lez')
            console.log('noononono' + name + ' ; ' + openslackmap.markers[i][NAME].replace(/\s+/g,''));
        if (name == openslackmap.markers[i][NAME] || name == openslackmap.markers[i][NAME].replace(/\s+/g,'')){
            console.log('yeeees');
            return openslackmap.markers[i];
        }
    }
    return null;
}

function showZoomMarker(m){

    removeBigMarker();
    addBigMarker(m);
    var title = m[NAME];
    var latlng = L.latLng(m[LAT], m[LON] );
    openslackmap.map.setView(latlng, 14);
    updateLineListFromBounds();


}

function removeHoverMarker(){
    if (openslackmap.currentHoverLayer != null){
        openslackmap.map.removeLayer(openslackmap.currentHoverLayer);
        openslackmap.currentHoverLayer = null;
    }
}

function addHoverMarker(m){
    //Extend the Default marker class
    var RedIcon = L.Icon.Default.extend({
        options: {
            iconUrl: 'images/marker-icon-violet.png' 
        }
    });
    var redIcon = new RedIcon();

    var title = m[NAME];
    var marker = L.marker(L.latLng(m[LAT], m[LON]), { title: title, icon: redIcon });
    openslackmap.map.addLayer(marker);
    openslackmap.currentHoverLayer = marker;

}

function removeBigMarker(){
    if (openslackmap.currentBigLayer != null){
        openslackmap.map.removeLayer(openslackmap.currentBigLayer);
    }
}

function addBigMarker(m){
    //Extend the Default marker class
    var RedIcon = L.Icon.Default.extend({
        options: {
            iconUrl: 'images/marker-icon-red.png' 
        }
    });
    var redIcon = new RedIcon();

    var title = m[NAME];
    var marker = L.marker(L.latLng(m[LAT], m[LON]), { title: title, icon: redIcon });
    marker.bindPopup(openslackmap.markersPopupTxt[title],{autoPan:true, maxWidth:400});
    openslackmap.map.addLayer(marker);
    openslackmap.currentBigLayer = marker;

    marker.openPopup();

}

function goThere(coord){
    // close popup
    openslackmap.map.closePopup();
    var lat = coord.split(',')[0];
    var lon = coord.split(',')[1];
    openslackmap.routeControl.getPlan().setWaypoints(
            [null,
            L.latLng(parseFloat(lat),parseFloat(lon))]);
    $('#sidebar').addClass('collapsed');
    $('#sidebar li.active').removeClass('active');
    //alert('Now define your starting point in the bottom right corner');
    $('input[placeholder=Start]').addClass('backred');

}

$(document).ready(function(){
    load();
    if (watermarkers != null){
        for (var i = 0; i < watermarkers.markers.length; i++) {
            watermarkers.markers[i].push('w');
            openslackmap.markers.push(watermarkers.markers[i]);
        }
        for (var i = 0; i < slackmarkers.markers.length; i++) {
            slackmarkers.markers[i].push('s');
            openslackmap.markers.push(slackmarkers.markers[i]);
        }
        for (var i = 0; i < longmarkers.markers.length; i++) {
            longmarkers.markers[i].push('l');
            openslackmap.markers.push(longmarkers.markers[i]);
        }
        for (var i = 0; i < highmarkers.markers.length; i++) {
            highmarkers.markers[i].push('h');
            openslackmap.markers.push(highmarkers.markers[i])
        }
        for (var i = 0; i < reuhighmarkers.markers.length; i++) {
            reuhighmarkers.markers[i].push('h');
            openslackmap.markers.push(reuhighmarkers.markers[i])
        }
        for (var i = 0; i < reulongmarkers.markers.length; i++) {
            reulongmarkers.markers[i].push('l');
            openslackmap.markers.push(reulongmarkers.markers[i])
        }
        for (var i = 0; i < reumidmarkers.markers.length; i++) {
            reumidmarkers.markers[i].push('h');
            openslackmap.markers.push(reumidmarkers.markers[i])
        }
        for (var i = 0; i < reuwatermarkers.markers.length; i++) {
            reuwatermarkers.markers[i].push('w');
            openslackmap.markers.push(reuwatermarkers.markers[i])
        }
        console.log('l' + openslackmap.markers.length)
        genPopupTxt();

        redraw();
        updateLineListFromBounds();
    }
    else{
        console.log('pas de marqueur');
    }
    // TODO make marker bigger
    $('body').on('mouseenter','#linetable tbody tr', function() {
        marker = findMarkerByName($(this).find('.linetablelink').attr('name'));
        removeHoverMarker()
        addHoverMarker(marker);
        if ($('#transparentcheck').is(':checked')){
            $('#sidebar').addClass('transparent');
        }
    });
    $('body').on('mouseleave','#linetable tbody tr', function() {
        $('#sidebar').removeClass('transparent');
        removeHoverMarker();
    });

    $('body').on('click','.linetablelink', function(e) {
        e.preventDefault();
        marker = findMarkerByName($(this).attr('name'));
        showZoomMarker(marker);
        openslackmap.routeControl.getPlan().setWaypoints([null,null]);
    });
    // keeping table sort order
    $('body').on('sortEnd','#linetable', function(sorter) {
        openslackmap.tablesortCol = sorter.target.config.sortList[0];
    });
    $('body').on('change','#displayclusters', function() {
        redraw();
    });
    document.onkeydown = checkKey;
    $('body').on('change','select#categoryselect', function() {
        redraw();
        updateLineListFromBounds();
    });

    // handle url parameters (permalink to track)
    var line = getUrlParameter('line');
    console.log("line "+decodeURI(line));
    if (typeof line !== 'undefined'){
        // TODO center on line
        marker = findMarkerByName(decodeURI(line));
        showZoomMarker(marker);
    }

    // fields in main tab
    // fields in filters sidebar tab
    $('#lenmin').spinner({
        min: 0,
        step:1,
    })
    $('#lenmax').spinner({
        min: 0,
        step:1,
    })
    $('#heimin').spinner({
        min: 0,
        step:1,
    })
    $('#heimax').spinner({
        min: 0,
        step:1,
    })
    $('#clearfilter').button({
        icons: {primary: "ui-icon-trash"}
    }).click(function(e){
        e.preventDefault();
        clearFiltersValues();
        redraw();
        updateLineListFromBounds();

    });
    $('#applyfilter').button({
        icons: {primary: "ui-icon-check"}
    }).click(function(e){
        e.preventDefault();
        redraw();
        updateLineListFromBounds();
    });
    $('body').on('click','.routebutton', function(e) {
        var coord = $(this).attr('coord');
        goThere(coord);
    });

    openslackmap.routeControl.on('routeselected', function() {
        $('.leaflet-routing-alt').each(function(){
            if (!$(this).hasClass('leaflet-routing-alt-minimized') && !$(this).hasClass('default-skin')){
                $(this).addClass('default-skin');
                $(this).customScrollbar();
                $(this).find('.scroll-bar').attr('style','display: block; height: 320px;');
                $(this).find('.viewport').attr('style','width: 308px; height: 320px;');
                console.log('aa');
            }
            else{
                // when we select another, scroll everybody to top
                if ($(this).hasClass('default-skin')){
                    $(this).customScrollbar("scrollToY", 0);
                }
            }
        });
    });
    $('body').on('focus','input[placeholder=Start]', function(e) {
        $(this).removeClass('backred');
    });
});
